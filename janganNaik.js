
/*
 *  Plugin NAme : janganNaik
 *  Create : Desember 24, 2013
 *  Create By : Chabib Nurozak
 *  version : 1.0
 */

(function($){
    $.fn.janganNaik = function(options){
        var el = this
        var defaults = {
            marginTop : 200
        },settings = $.extend({},defaults, options);
        var createEl = Math.floor((Math.random()*100)+1);
        el.after('<div class="tmpEl_'+createEl+'"></div>')
        var pDef = el.position()
        $(window).scroll(function(){
            var getTargetTop = pDef.top;
            //console.log("Scroll Top :" + getTargetTop)
            if($(window).scrollTop() >= (getTargetTop - settings.marginTop)){
                
                var p = el.position()
                //SEt CSS
                $(el).css({
                    'position' : 'fixed',
                    'left' : p.left+'px',
                    'width' : el.css('width'),
                    'top' : settings.marginTop
                })
                //console.log(p.margin)
                $(".tmpEl_"+createEl).css({
                    width : el.width(),
                    height : el.height(),
                    'display' : 'block'
                })
            }
            else {
                $(el).css({
                    'position' : '',
                    'left' : 'none',
                    'top' : settings.marginTop
                })
                $(".tmpEl_"+createEl).hide()
            }
        })
    }
})(jQuery)